<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/iniaslinya', function () {
    return view('welcome');
});


//--------------Intro Laravel----------//

Route::get('/', function () {
    return view('master');
});
Route::get('/form', 'RegisterController@form');
route::get('/welcome1', 'RegisterController@welcome1');
Route::post('/welcome1', 'RegisterController@welcome1_post');


//Memasangkan Template dengan Laravel Blade//
Route::get('/master', function(){
    return view('master');
});

route::get('/table', function(){
    return view('tabelfor.table');
});
route::get('/datatable', function(){
    return view('tabelfor.datatable');
});

Route::get('/cast','castController@index');
Route::get('/cast/create','castController@create');
Route::post('/cast','castController@store');
Route::get('/cast/{cast_id}', 'castController@show');
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('/cast/{cast_id}', 'castController@update');
Route::delete('/cast/{cast_id}', 'castController@destroy');