@extends('master')
@section('title')
    <h1> Halaman Tambah Cast</h1>
@endsection
@include ('lteadmin.partials.sidebar2')
@section('content')
<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="nama">
                <label>Umur</label>
                <input type="integer" class="form-control" name="umur"  placeholder="umur">
                <label>Bio</label>
                <input type="textarea" class="form-control" name="bio"  placeholder="bio">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
    
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection