@extends('master')
@section('title')
    <h1> Halaman Detail Cast</h1>
@endsection
@include ('lteadmin.partials.sidebar2')
@section('content')
<h2>Show cast dengan id {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection