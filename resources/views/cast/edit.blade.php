@extends('master')
@section('title')
    <h1> Halaman Edit Cast</h1>
@endsection
@include ('lteadmin.partials.sidebar2')
@section('content')
<div>
        <form action="/cast/{{$cast->id}}" method="POST">
           @method('put')
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="nama">
                <label>Umur</label>
                <input type="integer" class="form-control" name="umur" value="{{$cast->umur}}"  placeholder="umur">
                <label>Bio</label>
                <input type="textarea" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="bio">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
    
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection